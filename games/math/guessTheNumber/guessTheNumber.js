"use strict";
const assetsPath = "../../../../mimer-assets-pre/";

export class GuessTheNumber extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "InOrder", active: true });
    this.gameOn = true;
    this.score = 0;
    this.scoreMsg = "Score: ";
    this.scoreText;
    this.inputText;
    this.inputNumber;
  }

  preload() {}

  create() {
    this.scoreText = this.add.text(10, 10, this.scoreMsg + this.score, {
      fontSize: "32px",
      fill: "#FFF"
    });

    this.input.keyboard.on("keydown", function(event) {
      console.dir(event);
      event.preventDefault();
      return false;
    });
  }

  update() {}

  pressNumberHandler(number) {}
}
