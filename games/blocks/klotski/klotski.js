export class KlotskiScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "KlotskiScene", active: true });
    this.player;
    this.pen;
    this.gameOn = true;
    this.size = 100;
    this.padding = 5;
    this.origin = {};
    this.origin.x = 100;
    this.origin.y = 100;
  }

  create() {
    this.matter.world.setBounds();

    this.matter.add.rectangle(
      this.origin.x,
      this.origin.y,
      this.size,
      2 * this.size,
      { restitution: 0.9 }
    );
    this.matter.add.rectangle(
      this.origin.x + this.size + 2 * this.padding,
      this.origin.y,
      this.size,
      this.size,
      { restitution: 0.9 }
    );
    this.matter.add.rectangle(
      this.origin.x + 2 * this.size + 3 * this.padding,
      this.origin.y,
      this.size,
      this.size,
      { restitution: 0.9 }
    );
    this.matter.add.rectangle(
      this.origin.x + 3 * this.size + 4 * this.padding,
      this.origin.y,
      2 * this.size,
      2 * this.size,
      { restitution: 0.9 }
    );

    this.matter.add.mouseSpring();
  }
}
